package com.example.LoggingSystem.appender;

import com.example.LoggingSystem.LoggingSystemApplication;
import com.example.LoggingSystem.db.MongoDbConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.Serializable;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;


@Plugin(
        name = "MongoAppender",
        category = Core.CATEGORY_NAME,
        elementType = Appender.ELEMENT_TYPE,
        printObject = true)
public class MongoAppender extends AbstractAppender {

    private MongoDbConnection connection = null;

    private Queue<LogEvent> logQueue = new LinkedBlockingQueue<>();

    private Logger logger = LogManager.getLogger(LoggingSystemApplication.class);

    protected MongoAppender(String name, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
    }

    @Autowired
    public void setConnection(MongoDbConnection connection) {
        this.connection = connection;
    }

    @PluginFactory
    public static MongoAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Filter") Filter filter) {
        return new MongoAppender(name, filter, null);
    }

    @Override
    public void append(LogEvent event) {
        init();
        if (connection == null) {
            logQueue.add(event);
            return;
        }

        if (!logQueue.isEmpty()) {
            while (!logQueue.isEmpty()) {
                connection.writeToDatabase(logQueue.poll());
            }
        }
        connection.writeToDatabase(event);
    }

    private void init() {
        if (connection != null) {
            return;
        }

        ApplicationContext context = new AnnotationConfigApplicationContext(MongoDbConnection.class);

        try {
            connection = context.getBean(MongoDbConnection.class);
        } catch (BeansException e) {
            // nothing to do
        }
    }
}