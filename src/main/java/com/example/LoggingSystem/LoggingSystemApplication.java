package com.example.LoggingSystem;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class LoggingSystemApplication {

    private static final Logger logger = LogManager.getLogger(LoggingSystemApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(LoggingSystemApplication.class, args);

        logger.info("Info log message");
        logger.error("Error log message");
    }
}
