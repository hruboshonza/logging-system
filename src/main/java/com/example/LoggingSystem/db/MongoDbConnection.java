package com.example.LoggingSystem.db;

import com.example.LoggingSystem.config.MongoConfig;
import com.example.LoggingSystem.model.LogEntity;
import org.apache.logging.log4j.core.LogEvent;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;

import java.time.Instant;
import java.time.ZoneId;

@Service
@ContextConfiguration(classes = MongoConfig.class)
public class MongoDbConnection implements InitializingBean, DisposableBean {

    private MongoTemplate mongoTemplate;

    @Override
    public void afterPropertiesSet() throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(MongoConfig.class);
        mongoTemplate = context.getBean(MongoConfig.class).mongoTemplate();
    }

    public void writeToDatabase(LogEvent event) {

        LogEntity log = new LogEntity(
                Instant.ofEpochMilli(event.getTimeMillis()).atZone(ZoneId.systemDefault()).toLocalDateTime().toString(),
                event.getThreadName(),
                event.getLoggerName(),
                event.getLevel().toString(),
                event.getMessage().toString()
        );

        mongoTemplate.insert(log);
    }

    @Override
    public void destroy() throws Exception {
        mongoTemplate.dropCollection(LogEntity.class);
    }
}
