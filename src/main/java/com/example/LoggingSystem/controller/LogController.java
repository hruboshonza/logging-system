package com.example.LoggingSystem.controller;


import com.example.LoggingSystem.service.LogService;
import com.example.LoggingSystem.model.LogEntity;
import org.springframework.boot.logging.LogLevel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("logs")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping("all")
    public List<LogEntity> getAllLogs() {
        return logService.getAll();
    }

    @GetMapping("level/{level}")
    public List<LogEntity> getAllLogs(@PathVariable LogLevel level) {
        return logService.getByLevel(level);
    }

    @GetMapping("ftext/{text}")
    public List<LogEntity> getAllByFullTextSearch(@PathVariable String text) {
        return logService.findByFullText(text);
    }
}
