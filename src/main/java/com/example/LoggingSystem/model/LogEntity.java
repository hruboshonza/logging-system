package com.example.LoggingSystem.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document
public class LogEntity {

    @TextIndexed private String date;
    @TextIndexed private String threadName;
    @TextIndexed private String loggerName;
    @TextIndexed private String logLevel;
    @TextIndexed private String message;

    public LogEntity(String date, String threadName, String loggerName, String logLevel, String message) {
        this.date = date;
        this.threadName = threadName;
        this.loggerName = loggerName;
        this.logLevel = logLevel;
        this.message = message;
    }

    @Override
    public String toString() {
        return "LogEntity{" +
                "date='" + date + '\'' +
                ", threadName='" + threadName + '\'' +
                ", loggerName='" + loggerName + '\'' +
                ", logLevel='" + logLevel + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
