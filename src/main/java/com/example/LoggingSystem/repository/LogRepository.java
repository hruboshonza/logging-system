package com.example.LoggingSystem.repository;

import com.example.LoggingSystem.model.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LogRepository extends MongoRepository<LogEntity, String> {
}
