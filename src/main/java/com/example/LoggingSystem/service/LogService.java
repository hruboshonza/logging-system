package com.example.LoggingSystem.service;

import com.example.LoggingSystem.LoggingSystemApplication;
import com.example.LoggingSystem.model.LogEntity;
import com.example.LoggingSystem.repository.LogRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.logging.LogLevel;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogService {

    private static final Logger logger = LogManager.getLogger(LoggingSystemApplication.class);

    private final LogRepository logRepository;
    private final MongoTemplate logTemplate;

    public LogService(LogRepository logRepository, MongoTemplate template) {
        this.logRepository = logRepository;
        this.logTemplate = template;
    }

    public List<LogEntity> getAll() {
        logger.debug("Get all logs");
        return logRepository.findAll();
    }

    public List<LogEntity> getByLevel(LogLevel level) {
        Query query = new Query();
        query.addCriteria(Criteria.where("logLevel").is(level));

        return logTemplate.find(query, LogEntity.class);
    }

    public List<LogEntity> findByFullText(String text) {
        TextIndexDefinition textIndex = new TextIndexDefinition.TextIndexDefinitionBuilder()
                .onField("date")
                .onField("threadName")
                .onField("loggerName")
                .onField("logLevel")
                .onField("message")
                .build();
        logTemplate.indexOps(LogEntity.class).ensureIndex(textIndex);

        TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingPhrase(text);
        Query query = TextQuery.queryText(criteria).sortByScore();

        return logTemplate.find(query, LogEntity.class);
    }


}
